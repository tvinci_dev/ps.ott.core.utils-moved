##########################################################
#### This script will set all required variables  ########
##########################################################

# Remove echo
set +x
echo .
echo .
echo === Parse Solution ID from Git URL ===
basename=$(basename ${GIT_URL})
echo $basename
RepoName=${basename%.*}
echo $RepoName

echo .
echo .
echo === Cloning Repository ${GIT_URL} ${BRANCH} ===
git clone ${GIT_URL} -b ${BRANCH}
cd ${RepoName}

echo .
echo .
echo === Setting Default Build Variables ===
VERSION_TAG=$(git describe --always --dirty --long --tags)
VERSION_TAG=${VERSION_TAG/-dirty/}
VERSION_TAG=${VERSION_TAG//-/.}
VERSION_TAG_NUMBER=${VERSION_TAG::-9}

# Check if tag was not added to repository - if so, add 1.0
# if test -z "$VERSION_TAG_NUMBER" 
# then
# 	echo tag was not found, adding 1.0
# 	VERSION_TAG_NUMBER = "1.0"
# fi

# Check if SOLUTION_FILE_NAME is provided and overwrite SOLUTION_ID
if test -z "$SOLUTION_FILE_NAME" 
then
	echo SOLUTION_FILE_NAME param was not provided, assuming that solution file is set to ${RepoName}
    export SOLUTION_ID=${RepoName}
else
	echo Setting SOLUTION_ID to provided ${SOLUTION_FILE_NAME}
    export SOLUTION_ID=${SOLUTION_FILE_NAME}
fi

export SOLUTION_FILENAME="${SOLUTION_ID}.sln"
export PUBLISH_PROJECT_TARGET_DIR=${SOLUTION_ID}
export PUBLISH_PROJECT_TARGET_FILENAME="${SOLUTION_ID}.csproj"

export PUBLISH_DIR_LOCAL="/d/PSOTT_DEV/Releases/${SOLUTION_ID}"
export NUGET_REPO_GLOBAL="https://nuget.org/api/v2/"
export NUGET_REPO_PS="http://localhost:8090/nuget"
export BUILD_TARGET_DIR='.'


echo SOLUTION_ID = ${SOLUTION_ID}
echo VERSION_TAG = ${VERSION_TAG}
echo VERSION_TAG_NUMBER = ${VERSION_TAG_NUMBER}
echo SOLUTION_FILENAME = ${SOLUTION_FILENAME}
echo PUBLISH_PROJECT_TARGET_DIR = ${PUBLISH_PROJECT_TARGET_DIR}
echo PUBLISH_PROJECT_TARGET_FILENAME = ${PUBLISH_PROJECT_TARGET_FILENAME}
echo PUBLISH_DIR_LOCAL = ${PUBLISH_DIR_LOCAL}
echo NUGET_REPO_GLOBAL = ${NUGET_REPO_GLOBAL}
echo NUGET_REPO_PS = ${NUGET_REPO_PS}
echo BUILD_TARGET_DIR = ${BUILD_TARGET_DIR}

echo
echo
echo === Overwriting Build Variables from build.props.ini ===
if [ ! -f build.props.ini ]; then
    echo "build.props.ini file not found skipping overwrites!"
else
	config=$(cat build.props.ini)
	echo Loaded following overwrites
    echo $config
    echo $pwd
	source ./build.props.ini
fi


echo
echo
echo === Final Build Variables ===
echo SOLUTION_ID = ${SOLUTION_ID}
echo VERSION_TAG = ${VERSION_TAG}
echo VERSION_TAG_NUMBER = ${VERSION_TAG_NUMBER}
echo SOLUTION_FILENAME = ${SOLUTION_FILENAME}
echo PUBLISH_PROJECT_TARGET_DIR = ${PUBLISH_PROJECT_TARGET_DIR}
echo PUBLISH_PROJECT_TARGET_FILENAME = ${PUBLISH_PROJECT_TARGET_FILENAME}
echo PUBLISH_DIR_LOCAL = ${PUBLISH_DIR_LOCAL}
echo NUGET_REPO_GLOBAL = ${NUGET_REPO_GLOBAL}
echo NUGET_REPO_PS = ${NUGET_REPO_PS}
echo BUILD_TARGET_DIR = ${BUILD_TARGET_DIR}

cat > ${WORKSPACE}/generated.jenkins.vars <<EOL
RepoName=${RepoName}
SOLUTION_ID=${SOLUTION_ID}
VERSION_TAG=${VERSION_TAG}
VERSION_TAG_NUMBER=${VERSION_TAG_NUMBER}
SOLUTION_FILENAME=${SOLUTION_FILENAME}
PUBLISH_PROJECT_TARGET_DIR=${PUBLISH_PROJECT_TARGET_DIR}
PUBLISH_PROJECT_TARGET_FILENAME=${PUBLISH_PROJECT_TARGET_FILENAME}
PUBLISH_DIR_LOCAL=${PUBLISH_DIR_LOCAL}
NUGET_REPO_GLOBAL=${NUGET_REPO_GLOBAL}
NUGET_REPO_PS=${NUGET_REPO_PS}
BUILD_TARGET_DIR=${BUILD_TARGET_DIR}

... 
EOL
echo
echo