###############################
####### Nuget Restore  ########
###############################
# Remove echo
set +x
cd ${RepoName}
echo
echo
echo === Nuget Restore ===
/c/nuget.exe restore ${SOLUTION_FILENAME} -source ${NUGET_REPO_GLOBAL} -source ${NUGET_REPO_PS}

echo
echo


