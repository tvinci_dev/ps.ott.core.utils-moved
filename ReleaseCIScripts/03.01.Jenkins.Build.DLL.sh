######################################
####### Build  DLL Project    ########
######################################
# Remove echo
set +x
cd ${RepoName}

echo
echo
echo === Assembly Version Patch === 
DllVersioning.sh "./${PUBLISH_PROJECT_TARGET_DIR}/"

BUILD_PATH=$(sed 's/\//\\/g' <<< "${WORKSPACE}/${RepoName}/build");
echo
echo
echo === Publish to Disk ===
echo "Build path: ${BUILD_PATH}"
cd ${BUILD_TARGET_DIR}
"/c/Program Files (x86)\Microsoft Visual Studio\2019\Professional\MSBuild\Current\Bin\MsBuild.exe" "${PUBLISH_PROJECT_TARGET_DIR}/${PUBLISH_PROJECT_TARGET_FILENAME}" \
	//p:Configuration=Release \
    //p:Platform=AnyCPU \
    //t:WebPublish \
    //p:WebPublishMethod=FileSystem \
    //p:DeleteExistingFiles=True \
    //p:publishUrl="${BUILD_PATH}"