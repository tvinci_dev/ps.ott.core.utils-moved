##############################################
####### Build .Net Core Microservice  ########
##############################################
# Remove echo
set +x
cd ${RepoName}

echo
echo
echo === Assembly Version Patch === 
DllVersioning.Core.sh "./${PUBLISH_PROJECT_TARGET_DIR}/"


echo
echo
echo === Dotnet Restore ===
dotnet restore --source ${NUGET_REPO_GLOBAL} --source ${NUGET_REPO_PS}

echo
echo
echo === Publish to Disk ===
cd ${BUILD_TARGET_DIR}
dotnet publish -c Release -o "build" /property:Version=${VERSION_TAG_NUMBER}

echo
echo