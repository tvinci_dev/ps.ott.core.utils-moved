
# Remove echo
set +x
cd ${RepoName}

echo === Zip ===
rm -rf "${PUBLISH_DIR_LOCAL}/${VERSION_TAG}.zip"

echo === Echo Create version html file to build ===
echo $(date) ${VERSION_TAG} > "${WORKSPACE}/${RepoName}/build/version.html"

"/c/Program Files/7-Zip/7z.exe" a "${PUBLISH_DIR_LOCAL}/${VERSION_TAG}.zip" "${WORKSPACE}/${RepoName}/build/*"

echo === uploading to ifra-kdevops S3 ===
aws s3 cp "${PUBLISH_DIR_LOCAL}/${VERSION_TAG}.zip" s3://kaltura-ott-deployments-builds/adapters/${PUBLISH_PROJECT_TARGET_DIR}/ --profile infra-kdevops
 
echo
echo




cd ${RepoName}
echo === Clean ===
rm -rf "${PUBLISH_PROJECT_TARGET_DIR}/bin/"
rm -rf "${PUBLISH_PROJECT_TARGET_DIR}/build/"
echo
echo

echo === Release Artifacts ===
echo "external publish network path is: \\\\34.248.201.147\\PSOTT_DEV\\Releases\\${SOLUTION_ID}\\${VERSION_TAG}.zip"
echo "internal publish network path is: \\\\172.31.36.255\\PSOTT_DEV\\Releases\\${SOLUTION_ID}\\${VERSION_TAG}.zip"
echo "local publish network path is: ${PUBLISH_DIR_LOCAL}/${VERSION_TAG}.zip"

