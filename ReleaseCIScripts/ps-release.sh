#!/bin/bash
TOKEN='KalturaPSRocks'

GIT_URL='Unknown'
bitbucket_remote_host='git@bitbucket.org:tvinci_dev/'
github_remote_host='git@github.com:kaltura/'
fullOriginUrl=$(git config --get remote.origin.url)
echo fullOriginUrl $fullOriginUrl
fullOriginUrl=${fullOriginUrl/\/src/}

echo fullOriginUrl2 $fullOriginUrl

gitRepoName=$(basename $fullOriginUrl .git)
echo gitRepoName $fullOriginUrl


echo "Trying to determine git or bitbucket remote:[$fullOriginUrl]"
echo
shopt -s nocasematch
if [[ $fullOriginUrl =~ "github" ]]; then
    echo 'github'

    GIT_URL="$github_remote_host$gitRepoName.git"
elif [[ $fullOriginUrl =~ "bitbucket" ]]; then
    echo 'bitbucket'
    GIT_URL="$bitbucket_remote_host$gitRepoName.git"
else
    echo 'Cannot determine git remote source its not git nor bitbucket.'
    exit 125 
fi


echo "Trying to determine release project type"
projectFile=$(find -name "*.csproj" | head -n 1)
svcFile=$(find -name "*.svc" | head -n 1)
outputType=$(grep -oPm1 "(?<=<OutputType>)[^<]+" $projectFile)
isWebApplication=$(grep -om1 "349c5851-65df-11da-9384-00065b846f21" $projectFile)
TargetFramework=$(grep -oPm1 "(?<=<TargetFramework>)[^<]+" $projectFile)


echo "Project File:     ${projectFile}"
echo "SVC File:         ${hasSvcFile}"
echo "Output Type:      ${outputType}"
echo "TargetFramework:  ${TargetFramework}"
echo "isWebApplication:  ${isWebApplication}"

if [[ $outputType =~ "WinExe" ]]; then
    echo "Found OutputType=WinExe publishing as Windows Application project"
    SOLUTION_TYPE='WIN_EXE'
elif [[ $svcFile != "" ]]; then
    echo "Found svc file in solution, assuming WCF project."
    SOLUTION_TYPE='WCF'
elif [[ $isWebApplication != "" ]]; then
    echo "Found web application type in .csproj file in solution. publishing as WCF project."
    SOLUTION_TYPE='WCF'	
elif [[ $outputType =~ "Library" ]]; then
    echo "Found OutputType=Library in .csproj file in solution and not svc files, assuming DLL project."
    SOLUTION_TYPE='DLL'
elif [[ $outputType =~ "Exe" ]]; then
    echo "Found OutputType=Exe in .csproj file in solution and not svc files, assuming Exe project."
    SOLUTION_TYPE='DLL'
else
echo "Could not identify WCF or DLL project, assuming .Net Core"
    SOLUTION_TYPE='NET_CORE'
fi

SOLUTION_FILE_NAME=$(basename $(find -name "*.sln") .sln)
BRANCH=${1:-master}

echo
echo
echo "Variables are set to:"
echo "GIT_URL:            [$GIT_URL]"
echo "SOLUTION_FILE_NAME: [$SOLUTION_FILE_NAME]"
echo "BRANCH:             [$BRANCH]"
echo "SOLUTION_TYPE:      [$SOLUTION_TYPE]"

# TODO: move host to a parameter for easy update
requestUrl_internal_ip="http://172.31.36.255:8080/view/PS/job/PS.Release/buildWithParameters?token=${TOKEN}&GIT_URL=${GIT_URL}&BRANCH=${BRANCH}&SOLUTION_TYPE=${SOLUTION_TYPE}&SOLUTION_FILE_NAME=${SOLUTION_FILE_NAME}"
requestUrl_external_ip="http://34.248.201.147:8080/view/PS/job/PS.Release/buildWithParameters?token=${TOKEN}&GIT_URL=${GIT_URL}&BRANCH=${BRANCH}&SOLUTION_TYPE=${SOLUTION_TYPE}&SOLUTION_FILE_NAME=${SOLUTION_FILE_NAME}"


echo "calling url:[$requestUrl_external_ip]"
result=$(curl --request GET \
    --max-time 5 \
    --write-out %{http_code} \
    --silent \
    --output /dev/null \
    --url $requestUrl_external_ip)

echo result is: $result

if [[ $result != "201" ]]; then
    echo
    echo
    echo "Unable to connect using external ip, trying internal ip"
    echo "calling url:[$requestUrl_internal_ip]"
    result=$(curl --request GET \
        --max-time 5 \
        --write-out %{http_code} \
        --silent \
        --output /dev/null \
        --url $requestUrl_internal_ip)

    echo result is: $result
fi
